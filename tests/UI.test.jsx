import { render, screen, fireEvent } from '@testing-library/react';
import App from "../src/App";
import { describe, expect, it } from "vitest";

describe("TeenagerSimulator component", () => {
    it("should render the title", () => {
        render(<App />);
        expect(screen.getByRole('heading', { name: /Teenage Boy Speaking/i })).toBeInTheDocument();
    });


    it("should render the image", () => {
        render(<App />);
        expect(screen.getByAltText("marco")).toBeInTheDocument();
    });

    it("should render the input field", () => {
        render(<App />);
        expect(screen.getByPlaceholderText("Type something...")).toBeInTheDocument();
    });

    it("should render the submit button", () => {
        render(<App />);
        expect(screen.getByRole('button', { name: 'Submit' })).toBeInTheDocument();
    });

    it("should handle empty input correctly", () => {
        render(<App />);
        const input = screen.getByPlaceholderText("Type something...");
        const button = screen.getByRole("button", { name: "Submit" });
        fireEvent.change(input, { target: { value: '' } });
        fireEvent.click(button);
        expect(screen.getByText("Bien, on fait comme ça !")).toBeInTheDocument();
    });

    it("should handle uppercase input without a question mark correctly", () => {
        render(<App />);
        const input = screen.getByPlaceholderText("Type something...");
        const button = screen.getByRole("button", { name: "Submit" });
        fireEvent.change(input, { target: { value: 'HELLO' } });
        fireEvent.click(button);
        expect(screen.getByText("Whoa, calmos !")).toBeInTheDocument();
    });

    it("should handle uppercase input with a question mark correctly", () => {
        render(<App />);
        const input = screen.getByPlaceholderText("Type something...");
        const button = screen.getByRole("button", { name: "Submit" });
        fireEvent.change(input, { target: { value: 'HELLO?' } });
        fireEvent.click(button);
        expect(screen.getByText("Calmez-vous, je sais ce que je fais !")).toBeInTheDocument();
    });

    it("should handle lowercase question correctly", () => {
        render(<App />);
        const input = screen.getByPlaceholderText("Type something...");
        const button = screen.getByRole("button", { name: "Submit" });
        fireEvent.change(input, { target: { value: 'hello?' } });
        fireEvent.click(button);
        expect(screen.getByText("Bien sûr")).toBeInTheDocument();
    });

    it("should handle any other input correctly", () => {
        render(<App />);
        const input = screen.getByPlaceholderText("Type something...");
        const button = screen.getByRole("button", { name: "Submit" });
        fireEvent.change(input, { target: { value: 'hello there' } });
        fireEvent.click(button);
        expect(screen.getByText("Peu importe !")).toBeInTheDocument();
    });
});
