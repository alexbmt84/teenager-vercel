import {useState} from 'react';

function App() {
    const [input, setInput] = useState('');
    const [response, setResponse] = useState('');

    const handleChange = (event) => {
        setInput(event.target.value);
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        let trimmedInput = input.trim();

        const endsWithQuestionMark = /\?\s*$/.test(trimmedInput);
        const upperCaseQuestionMark = /^[A-Z\d\s!]+\?+$/.test(trimmedInput);
        const allUpperCase = /^[A-Z\d\s!]+$/.test(trimmedInput);
        const emptyMessage = trimmedInput === "";

        if (upperCaseQuestionMark) {
            setResponse('Calmez-vous, je sais ce que je fais !');
        } else if (endsWithQuestionMark) {
            setResponse('Bien sûr');
        } else if (allUpperCase) {
            setResponse('Whoa, calmos !');
        } else if (emptyMessage) {
            setResponse('Bien, on fait comme ça !');
        } else {
            setResponse('Peu importe !');
        }


    };

    return (
        <div className="container">
            <div className="row my-5">
                <div className="col w-100">
                    <h1 className={"text-center"}>Teenage Boy Speaking</h1>
                    <div className={"d-flex justify-content-center mt-5"}>
                        <img src={"/marco.jpg"} alt={"marco"} width={600} height={600} className={"mt-5"}/>
                        <div className="mt-5 thought">
                            <p>{response}</p>
                        </div>
                    </div>
                    <form onSubmit={handleSubmit} className={"text-center mt-5 form-group"}>
                        <div className={"col-xs-3"}>
                            <input
                                type="text"
                                className={"form-control col-3"}
                                value={input}
                                onChange={handleChange}
                                placeholder="Type something..."
                            />
                        </div>
                        <button type="submit" className={"btn btn-dark mb-2"}>Submit</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default App;
